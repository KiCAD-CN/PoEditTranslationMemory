# PoEdit 翻译记忆

#### 介绍
PoEdit 翻译记忆

#### 软件架构
软件架构说明


#### 安装教程和使用说明

1.  [下载 Poedit](https://poedit.net/) 
2.  打开 Poedit 

- 编辑
- 首选项
- 翻译记忆
- 管理
- 导入 TMX...
- 选择本仓库的 `PoEdit 翻译记忆.tmx`文件
- 完成
![PoEdit 翻译记忆设置](PoEditTranslationMemorySet.png)


3.  本 PoEdit 翻译记忆存的 [KiCad-doc](https://gitlab.com/kicad/services/kicad-doc) 和 [KiCad-i18n](https://gitlab.com/kicad/code/kicad-i18n) 的翻译记忆。
4. [KiCad-CN](https://gitee.com/KiCAD-CN/) 码云地址：[KiCad-doc](https://gitee.com/KiCAD-CN/kicad-doc) [KiCad-i18n](https://gitee.com/KiCAD-CN/kicad-i18n)


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交使用后的翻译记忆文件
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
